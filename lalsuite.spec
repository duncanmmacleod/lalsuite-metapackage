%define lalsuite_version      7.3
%define release               3
%define lal_version           7.1.6
%define lalframe_version      1.6.1
%define lalmetaio_version     2.1.1
%define lalsimulation_version 3.1.1
%define lalburst_version      1.5.11
%define lalinspiral_version   2.0.5
%define lalpulsar_version     3.1.2
%define lalinference_version  3.0.2
%define lalapps_version       8.0.0

Name: lalsuite
Version: %{lalsuite_version}
Release: %{release}%{?dist}
License: GPLv2+
URL: https://git.ligo.org/packaging/rhel/lalsuite
Summary: LSC Algorithm Library Suite
BuildArch: noarch

BuildRequires: python3-pip

Requires: lal = %{lal_version}
Requires: lalframe = %{lalframe_version}
Requires: lalmetaio = %{lalmetaio_version}
Requires: lalsimulation = %{lalsimulation_version}
Requires: lalburst = %{lalburst_version}
Requires: lalinspiral = %{lalinspiral_version}
Requires: lalpulsar = %{lalpulsar_version}
%if 0%{?rhel} < 8
Requires: lalinference = %{lalinference_version}
Requires: lalapps = %{lalapps_version}
%endif
# replace old lscsoft-lalsuite meta-package
Provides: lscsoft-lalsuite = 20191213-2
Obsoletes: lscsoft-lalsuite < 20191213-2
%description
The LSC Algorithm Library Suite for gravitational wave data analysis.
This package provides all the different packages for the various
LALSuite component packages.

%package -n %{name}-devel
Summary: LSC Algorithm Library Suite - Development
Requires: liblal-devel = %{lal_version}
Requires: liblalframe-devel = %{lalframe_version}
Requires: liblalmetaio-devel = %{lalmetaio_version}
Requires: liblalsimulation-devel = %{lalsimulation_version}
Requires: liblalburst-devel = %{lalburst_version}
Requires: liblalinspiral-devel = %{lalinspiral_version}
Requires: liblalpulsar-devel = %{lalpulsar_version}
%if 0%{?rhel} < 8
Requires: liblalinference-devel = %{lalinference_version}
%endif
%description devel
This meta-package provides all the different development packages for
the various LALSuite component packages.

%package -n %{name}-octave
Summary: LSC Algorithm Library Suite - Octave Bindings
Requires: lal-octave = %{lal_version}
Requires: lalframe-octave = %{lalframe_version}
Requires: lalmetaio-octave = %{lalmetaio_version}
Requires: lalsimulation-octave = %{lalsimulation_version}
Requires: lalburst-octave = %{lalburst_version}
Requires: lalinspiral-octave = %{lalinspiral_version}
Requires: lalpulsar-octave = %{lalpulsar_version}
%if 0%{?rhel} < 8
Requires: lalinference-octave = %{lalinference_version}
%endif
%description octave
This meta-package provides all the different Octave packages for
the various LALSuite component packages.

%prep

%build

%install
install -m 755 -d %{buildroot}%{python3_sitelib}/%{name}-%{version}-py%{python3_version}.egg-info
cat << EOF > %{buildroot}%{python3_sitelib}/%{name}-%{version}-py%{python3_version}.egg-info/PKG-INFO
Name: %{name}
Version: %{version}
Summary: %{summary}
Author: LIGO Scientific Collaboration
Author-email: lal-discuss@ligo.org
Home-Page: %{url}
License: %{license}
Provides: %{name}
EOF

%check
# validate that pip thinks that lalsuite is already installed
PYTHONPATH=%{buildroot}%{python3_sitelib}:${PYTHONPATH} %{__python3} -m pip show %{name}

%files -n %{name}
%{python3_sitelib}

%files -n %{name}-devel
%files -n %{name}-octave

# dates should be formatted using: 'date +"%a %b %d %Y"'
%changelog
* Mon Jan 24 2022 <adam.mercer@ligo.org> 7.3-3
- Add LALBurst, LALInspiral, and LALPulsar dependencies for Rocky

* Fri Jan 14 2022 <adam.mercer@ligo.org> 7.3-2
- Only LAL, LALFrame, LALMetaIO, and LALSimulation are currently packaged for EL8, only add the dependencies on the other packages for EL7

* Thu Jan 13 2022 <adam.mercer@ligo.org> 7.3-1
- Update for 7.3

* Mon Dec 13 2021 <adam.mercer@ligo.org> 7.2-2
- Fix versions for 7.2

* Mon Dec 13 2021 <adam.mercer@ligo.org> 7.2-1
- Update for 7.2

* Thu Nov 18 2021 <adam.mercer@ligo.org> 7.1-1
- Update for 7.1

* Wed Aug 11 2021 <adam.mercer@ligo.org> 7.0-1
- Update for 7.0

* Wed Jan 27 2021 <adam.mercer@ligo.org> 6.81-1
- Update for 6.81

* Tue Jan 12 2021 <adam.mercer@ligo.org> 6.80-1
- New meta-packages for LALSuite that provides the explict LALSuite
  component versions for the specific LALSuite version.
