PACKAGE=$(shell echo *.spec | sed -e 's/.spec$$//')

sources: ;

srpm: sources
	rpmbuild -D '_srcrpmdir ./' -D '_sourcedir ./' -bs $(PACKAGE).spec
